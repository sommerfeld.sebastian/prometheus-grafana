#!/bin/bash
# @file stop.sh
# @brief Stop and remove prometheus and grafana containers.
#
# @description The script stops all prometheus and grafana containers. All containers, networks and volumes are
# removed after shutdown.
#
# ==== Arguments
#
# The script does not accept any parameters.

echo -e "$LOG_INFO shutting down"
docker-compose down -v

echo -e "$LOG_DONE ------------------------------------------------------------------"
echo -e "$LOG_DONE stopped and removed containers, networks and volumes"
echo -e "$LOG_DONE ------------------------------------------------------------------"
