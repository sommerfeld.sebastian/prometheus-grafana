#!/bin/bash
# @file logs.sh
# @brief Show logs for all containers.
#
# @description The scripts shows logs for all configured containers.
#
# ==== Arguments
#
# The script does not accept any parameters.

echo -e "$LOG_INFO showing logs"
docker-compose logs -f
