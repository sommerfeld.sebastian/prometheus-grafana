#!/bin/bash
# @file start.sh
# @brief Start Prometheus and Grafana in containers.
#
# @description The script starts Prometheus, Grafana and some other services in containers.
#
# | Container Name    | Software           | User/Pass   | Port | Protocol |
# | ----------------- | ------------------ | ----------- | ---- | -------- |
# | prometheus_server | Prometheus         | -/-         | 9090 | http     |
# | grafana           | Grafana            | admin/admin | 3000 | http     |
# | blackbox_exporter | blackbox-exporter  | -/-         | 9115 | http     |
#
# ==== Arguments
#
# The script does not accept any parameters.

echo -e "$LOG_INFO starting containers"
docker-compose up -d

echo -e "$LOG_INFO ------------------------------------------------------------------"
echo -e "$LOG_INFO startup in progress (detached)"
echo -e "$LOG_INFO ------------------------------------------------------------------"
